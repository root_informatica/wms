#!/bin/sh

## rename terminals ##
# wms_termrename.sh by @root_informatica.

. /tmp/wms_var

# focused window.
FW=$(pfw)

# if any terminal is focused.
if [ "$(atomx WM_CLASS $FW)" = "$TRM" ]; then
    # change atom name.
    atomx WM_NAME="$($XM < $NM)" $FW
fi

