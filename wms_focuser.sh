#!/bin/sh

## give windows the focus and put some color ##
# wms_focuser.sh by @root_informatica.

. /tmp/wms_var

INPUT=$1
# focused window. 
FW=$(pfw)

usage() {
    cat<<EOF
usage:
wms_mainrole.sh [ -n, -p, -t, wid ]
-n) next in order stack
-p) prev in order stack
-t) focus the top window in the server’s window stack
wid) window id
EOF
}

# focus next window in server's window stack order
next_window() {
    wid=$(lsw | grep -v $FW | sed '1 p;d')
    chwso -r $wid
}

# focus previous window in in server's window stack order
prev_window() {
    wid=$(lsw | grep -v $FW | sed '$ p;d')
    chwso -r $wid
}

# focus the top window in the server's window stack order
top_window() {
    wid=$(lsw | sed '$ p;d')
}

# focus on wid
on_wid() {
    wattr $INPUT && wid=$INPUT
    chwso -r $wid
}

case $INPUT in
    -n)
	next_window
	;;
    -p)
	prev_window
	;;
    -t)
	top_window
	;;
    0x*)
	on_wid
	;;
    *)
	usage
	;;
esac

# transfer focus
wtf $wid

# set colors on the unfocused windows.
chwb -s $BW -c $IC $(lsw | grep -v $wid)
# set colors on the focused widnow.
chwb -s $BW -c $AC $wid
