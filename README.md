# [ wms ]
[w]indow [m]manipulation [s]hellscripts.

A bunch of shellscripts around **wmutils**.

Well, not only is wms not a wm per se, it's also not an original project. What is it then you ask. It's my implementation of some tools from the wmutils project in a **bunch of horrible shell scripts**. As a clear requirement, you should have the wmutils binaries. Namely:
https://github.com/wmutils. 


Also wms is a derivative of [ ! wm]:
https://git.disroot.org/root_sti/nowm.git (dead at birth)



This one is designed to maintain and incorporate from that one, things that are essential for my workflow. Also, this must contain future experiments around the configuration of my scripts.
I am learning, discovering, experimenting. This makes these scripts very volatile.
Some of the scripts have other crap as dependencies which I have made available at:
https://git.disroot.org/root_sti/others.git

**FUNDAMENTALS** \
I have been seeing computer users for years struggling to make the most of their equipment's resources against a delirious trend of uncontrolled software inflammation. The consumption of inflated programs with hollow utilities and pure aesthetics and the proliferation of redundant layers that turn the simplest task into a labyrinth of abstractions. I don't want the same for my private life. So I started removing and abandoning. In the last two years I have managed to get them back to me, time and energy. Things that I now use for my personal growth. \
Two great decisions that may seem like errors but are rather conditioning from which I cannot escape since in principle they move me towards their truth. To save: the use of Xterm as a term emulator is such an unorthodox launcher. \
The first choice is based on the fact that in many distros Xterm is installed together with Xorg and if not, it is the terminal emulator closest to Xorg as a project. I could use st or urxvt, but it would be moving towards another paradigm and breaking the conceptual homogeneity of this project. \
The second option is based on the extension of the first and the fact of using what is already installed. Since fzy/fzy is present in many scripts taking the place of a terminal menu, extending its operation and putting it in a resized terminal as a launcher is more than sensible. \
I have left the three main scripts, which are the ones I use daily, as the fundamental option. I've put the others, which are mostly experimental features, in a folder called opt, for optional.

**My thanks and credits to the wmutils project. I wish you luck on your way.**


**THE CORE SCRIPTS:**

[wms.sh] = general purpose script to get everything wms-related up and runing.

[wms_voyeur.sh] = snoping on xorg window events and doing some things.

[wms_focuser.sh] = give windows the focus and put some color.

[wms_usher.sh] = move and resize windows.


**THE OPT SCRIPTS**

[wms_switch.sh] = switch between windows.

[wms_shepperd.sh] = sort windows by heards/flock.

[wms_layout.sh] = some layouts to order the windows.

[wms_bottom.sh] = send windows to desktop background.

[wms_toggle.sh] = toggle visivility of windows.

**THE FETICHES**

[wms_batlarm.sh] = alternating window border colors depending on battery charge.

[wms_termrename.sh] = rename terminals.

[wms_screensaver.sh] = use the windows on the desktop as screensavers by moving them randomly.


**SETUP STEPS** \
1 - download, compile and install wmutils/core and wmutils/opt.

2 - clone this repo and put at least the main scripts in some executable path.

3 - install a daemon for keyboard events, I have used sxhkd. Copy your configuration file, sxhkdrc, from wms/config to .config/sxhkd/.

5 - add the following lines in .xinitrc or sxrc: \
  exec wms.sh \

!! In the case of using a Display Manager, copy the wms.desktop file contained in wms/config to /usr/share/xsessions.


**This spawn doesn't have mouse support** \
**Sorry for the verbosity of the comments, the idea is to explain each script in general terms so that it can be easier to hack them**
