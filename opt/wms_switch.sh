#!/bin/sh

## switch between windows ##
# wms_switch.sh by @root_informatica.

. /tmp/wms_var

# all managed windows.
AMW=$(lsw)
# delay.
DELAY=.2

# if any maped window exist.
if [ -n "$AMW" ]; then
    # print id, class and name in xmenu.
    target=$(\
	     for wid in $(lsw); do
		 printf '%s\n' "$wid | $(atomx WM_CLASS $wid) | $(wname $wid)"
	     done | cut -c 1-100 | $XM | cut -d '|' -f 1)

sleep $DELAY &&
wms_focuser.sh "$target"    # focus target
fi
