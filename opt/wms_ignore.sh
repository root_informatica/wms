#!/bin/sh

## set windows so that they are ignored by the rest of the scripts ##
# it depends of wms_focuser.sh.
# wms_ignore.sh by @root_informatica.

. /tmp/wms_var

# focused window.
FW=$(pfw)
# ignored window id.
IW=$(lsw -o)

# set ignored window.
ignore() {
    # put it on bottom in window stack order. 
    chwso -l $FW
    # change border color to BC.
    chwb -c $BC $FW
    # ignore focused window.
    ignw -s $FW
    # focus prev window. 
    wms_focuser.sh -p
}

# unignore window.
restore() {
    # put it on top in the window stack order.
    chwso -r $IW
    # unignore. 
    ignw -r $IW
    # focus it.
    wms_focuser.sh $IW
}

if [ -n "$IW" ]; then
    restore

else
    ignore
fi
