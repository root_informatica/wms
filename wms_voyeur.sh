#!/bin/sh

## snooping on xorg window events and doing some things ##
## based on https://github.com/wmutils/contrib/blob/master/focus_watcher.sh ##
# wms_voyeur.sh by @root_informatica.

wew | while read ev wid args; do
    case $ev in
        MAP)    # focus on mappng requests.
	    wattr o $wid || wms_focuser.sh -t
	    ;;
	UNMAP)    # focus the top window in the server’s window stack.
	    [ ! "$(pfw)" ] && wms_focuser.sh -t
	    ;;
    esac
done


