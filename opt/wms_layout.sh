#!/bin/sh

## some layouts to order the windows ##
# wms_layout.sh by @root_informatica.

. /tmp/wms_var

FLAG=$1
# focused window.
FW=$(pfw)
# all managed windows.
AMW=$(lsw)
# maped windows count.
MWC=$(lsw | wc -l)
# wms_var path.
WMSVAR="/tmp/wms_var"

usage() {
    cat<<EOF
usage:
wms_usher.sh [ -m, -t, -w ]
-m) monucule
-t) tiled
-w) widespread
EOF
}

# all windows at full screen
monocule_layout() {
    for wid in $AMW; do
	wtp 0 0 $SW $SH $wid
    done
}

# tiling. Master and stack
tiling_layout() {
    # screen widht. gaps added.
    screen_widht=$((RW -  2 * GAP - 2 * BW))
    # master screen height.
    master_screen_height=$((RH - 2 * GAP - 2 * BW))
    # stack screen height (( - BW))?
    stack_screen_height=$((RH - GAP))
    # stack windows count.
    stack_windows_count=$((MWC - 1))
    # master area width.
    master_area_width=$((screen_widht * MP / 100 - 2 * BW))
    # master area height.
    master_area_height=$master_screen_height
    # master X coordinate.
    master_x=$GAP
    # master Y coordinate.
    master_y=$GAP
    # stack area width.
    stack_area_width=$((screen_widht - master_area_width - GAP - 2 * BW))

    # stack area height.
    # if swcount is 0 or not. 
    [ "$stack_windows_count" = "0" ] && stack_area_height=stack_screen_height \
	    || stack_area_height=$((stack_screen_height / stack_windows_count - GAP - 2 * BW))

    # stack x coordinate.
    stack_x=$((master_area_width + 2 * GAP + 2 * BW))
    # stack y coordinate.
    stack_y=$GAP

    if [ "$stack_windows_count" = "0" ]; then
	wtp $master_x $master_y $screen_widht $master_screen_height $FW
    else
	# put focused window as master
	wtp $master_x $master_y $master_area_width $master_area_height $FW
	
	# put the rest of the windows in stack
	for wid in $(lsw | grep -v $FW); do
	    wtp $stack_x $stack_y $stack_area_width $stack_area_height $wid
	    # incremental stack Y coordinate.
	    stack_y=$((stack_y + stack_area_height + GAP + 2 * BW))
	done
    fi
}

# widespread
widespread_layout() {
    # windows width.
    window_width=$((SW * WP / 100))
    # windows height.
    window_height=$((SH * WP / 100))
    # initial X coordinate.
    x=$(((RW - window_width) / (MWC * 2)))
    # initial Y coordinate.
    y=$(((RH - window_height) / (MWC * 2)))
    # function X value.
    x_max=$(((RW - window_width) / MWC))
    # function Y value.
    y_max=$(((RH - window_height) / MWC))

    # put windows in cascade
    for wid in $AMW; do
	wtp $x $y $window_width $window_height $wid
	# incremental X value.
	x=$((x + x_max))
	# incremental Y value.
	y=$((y + y_max))
    done
}

# increase gaps. (+2 pixels)
gap_inc() {
    [ $GAP -le 40 ] && \
	sed -i "s/^.*\bGAP=\b.*$/GAP=$((GAP + 2))/" $WMSVAR
}

 # decrease gaps. (-2 pixels)
gap_dec() {
    [ $GAP -ge 2 ] && \
	sed -i "s/^.*\bGAP=\b.*$/GAP=$((GAP - 2))/" $WMSVAR
}

 # increase master area. (+5 pixels)
master_inc() {
    [ $MP -le 70 ] && \
	sed -i "s/^.*\bMP=\b.*$/MP=$((MP + 5))/" $WMSVAR
}

 # decrease master area. (-5 pixels)
master_dec() {
    [ $MP -ge 40 ] && \
	sed -i "s/^.*\bMP=\b.*$/MP=$((MP - 5))/" $WMSVAR
}

 # increase window percentage. (+5 %)
windowperc_inc() {
    [ $WP -le 90 ] && \
	sed -i "s/^.*\bWP=\b.*$/WP=$((WP + 5))/" $WMSVAR
}

 # decrease window percentage. (-5 %)
windowperc_dec() {
    [ $WP -ge 60 ] && \
	sed -i "s/^.*\bWP=\b.*$/WP=$((WP - 5))/" $WMSVAR
}

# if there is a focused window.
if [ -n "$FW" ]; then
    case $FLAG in
	-m)
	    monocule_layout
	    ;;
	-t)
	    tiling_layout
	    ;;	    
	-w)
	    widespread_layout
	    ;;
	-a)
	    master_dec
	    . /tmp/wms_var && tiling_layout
	    ;;
	-A)
	    master_inc
	    . /tmp/wms_var && tiling_layout
	    ;;
	-g)
	    gap_dec
	    . /tmp/wms_var && tiling_layout
	    ;;
	-G)
	    gap_inc
	    . /tmp/wms_var && tiling_layout
	    ;;
	-p)
	    windowperc_dec
	    . /tmp/wms_var && widespread_layout
	    ;;
	-P)
	    windowperc_inc
	    . /tmp/wms_var && widespread_layout
	    ;;
	*)
	    usage
	    ;;
    esac
fi


