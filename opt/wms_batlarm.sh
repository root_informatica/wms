#!/bin/sh

## alternating window border colors depending on battery charge ##
# this script depends on power.sh which is in the /others repo.
# wms_batlarm.sh by @root_informatica.

. /tmp/wms_var

# freq.
FREQ=0.5
# alternate colors.
COLORS="aa0000 $AC"

while true; do
    # battery status.
    read -r STATUS < /sys/class/power_supply/BAT0/status
    # if status discharging.
    if [ "$STATUS" = "Discharging" ]; then
	# alternate border colors.
	for c in $COLORS; do
	    chwb -c $c $(pfw)
	    sleep $FREQ
	done

    else    # if not
	# default border colors.
	chwb -c $AC $(pfw)
	# exit loop.
	break

    fi
done
