#!/bin/sh

## toggle visivility of windows ##
# wms_toggle.sh by @root_informatica.

FLAG=$1
# focused window.
FW=$(pfw)

usage() {
    cat<<EOF
usage: wms_toggle.sh [-t,-u ]
-t) toggle map.
-u) unmap widnow.
EOF
}

unmap() {
    atomx WM_TOGGLE="$FW" $FW
    mapw -u $FW
}

toggle() {
    for wid in $(lsw); do
	     atomx WM_TOGGLE="$wid" $wid
	     done
    mapw -t $(atomx WM_TOGGLE $(lsw -a))
}

case $FLAG in
    -t)
	toggle
	;;
    -u)
	unmap
	;;
    *)
	usage
	;;
esac    
    
