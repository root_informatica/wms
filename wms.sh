#!/bin/sh

## general purpose script to get everything wms-related up and running. ##
# some (all) of the contents are capricious so feel free to change whatever you want.
# variables are defined redundantly to make them clearer to understand
# wms.sh by @root_informaica

# first a little art.
WMS_ART='"
    ▄    ▄▄▄▄▄▄▄    ▄   
   ▀▀▄ ▄█████████▄ ▄▀▀  
       ██ ▀███▀ ██      
     ▄ ▀████▀████▀ ▄    
   ▀█    ██▀█▀██    █▀  
 
     █   █ █▄ ▄█ ▄▀▀    
     ▀▄▀▄▀ █ ▀ █ ▄██

"'
# some very personal things?
WM=wms
TERMINAL=xterm
XMENU=xmenu.sh
ACTIVE_COLOR=a0a0a0
INACTIVE_COLOR=404040
BACKGROUND_COLOR=101010
BORDER_WIDTH=2
GAP=6
WINDOWS_PERCENTAGE=94
MASTER_PERCENTAGE=60

NAMES="emacs
profanity
nchat
toxic
irssi
music
podcast
rtorrent
"
# some math.
ROOT_ID=$(lsw -r)
ROOT_WIDTH=$(wattr w $ROOT_ID)
ROOT_HEIGHT=$(wattr h $ROOT_ID)
SCREEN_WIDTH=$((ROOT_WIDTH - 2 * BORDER_WIDTH))
SCREEN_HEIGHT=$((ROOT_HEIGHT - 2 * BORDER_WIDTH))

# a very brief pointer for root-window. (opcional?)
CURSOR="
#define wms_width 10
#define wms_height 10
static unsigned char wms_bits[] = {
   0x00, 0x00, 0x78, 0x00, 0xfc, 0x00, 0xfe, 0x01, 0xfe, 0x01, 0xfe, 0x01,
   0xfe, 0x01, 0xfc, 0x00, 0x78, 0x00, 0x00, 0x00 };
"

# a super simple desktop wallpaper
BITMAP="
#define wms_width 32
#define wms_height 23
static unsigned char wms_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x40, 0xf0, 0x0f, 0x02, 0x40, 0xf8, 0x1f, 0x04, 0x80, 0xfc, 0x3f, 0x01,
   0x00, 0xee, 0x37, 0x00, 0x00, 0xcc, 0x73, 0x00, 0x00, 0xce, 0x33, 0x00,
   0x00, 0xfc, 0x7f, 0x00, 0x80, 0x78, 0x1e, 0x01, 0x20, 0xf0, 0x0f, 0x04,
   0x40, 0xb0, 0x0d, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
"

# make general variables insanely short and accessible to all wms scripts.
printf "
ART=$WMS_ART
TRM=$TERMINAL
XM=$XMENU
AC=$ACTIVE_COLOR
IC=$INACTIVE_COLOR
BC=$BACKGROUND_COLOR
BW=$BORDER_WIDTH
WP=$WINDOWS_PERCENTAGE
MP=$MASTER_PERCENTAGE
GAP=$GAP
NM="/tmp/wms_names"
RW=$ROOT_WIDTH
RH=$ROOT_HEIGHT
SW=$SCREEN_WIDTH
SH=$SCREEN_HEIGHT
" > /tmp/wms_var

# give a good name to the work environment.
atomx WM_NAME=$WM $ROOT_ID > /dev/null 2>&1

# create the names file.
printf "$NAMES" > /tmp/wms_names

# start the keyboard daemon listener.
sxhkd &

# ensure cursor and bitmap.
# This is designed so that wms is the least invasive within the user space.
[ ! -f $HOME/.cursor.xbm ] && \
    printf "$CURSOR" > /tmp/cursor.xbm && \
    cursor_path="/tmp/cursor.xbm" \
	  || cursor_path="$HOME/.cursor.xbm"

[ ! -f $HOME/.bitmap.xbm ] && \
    printf "$BITMAP" > /tmp/bitmap.xbm && \
    bitmap_path="/tmp/bitmap.xbm" \
	  || bitmap_path="$HOME/.bitmap.xbm"

# Time to make the cursor and image effective.
xsetroot -fg "#$ACTIVE_COLOR" -bg "#$BACKGROUND_COLOR" -cursor $cursor_path $cursor_path
xsetroot -fg "#$INACTIVE_COLOR" -bg "#$BACKGROUND_COLOR" -bitmap $bitmap_path

# finaly, start the xorg event listener.
wms_voyeur.sh
