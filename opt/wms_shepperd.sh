#!/bin/sh

## sort windows by herds/flock ##
# wms_shepperd.sh by @root_informatica.

. /tmp/wms_var

FLAG=$1
# focused window.
FW=$(pfw)

usage() {
    cat<<EOF
usage:
wms_shepperd.sh [ -a, -A, -d, -D, -t ]
-a) add window
-A) add all
-d) delete window
-D) delete all
-t) togle
EOF
}

# create atom in focused window.
add_focused_window() {
    # chose herd name.
    herd_name="$($XM < $NM)"
    if [ -n "$herd_name" ]; then
	atomx WM_HERD="$herd_name $FW" $FW
    else
	exit 0
    fi
}

# create atom in all maped windows.
add_all_windows() {
    # chose herd name.
    herd_name="$($XM < $NM)"
    for wid in $AMW; do
	if [ -n "$herd_name" ]; then
	    atomx WM_HERD="$herd_name $wid" $wid
	else
	    exit 0
	fi
    done
}

# delete atom from focused window
delete_focused_window() {
    atomx -d WM_HERD $FW
}

delete_all_windows() {
    atomx -d WM_HERD $AMW
}

# togle herds.
togle_herds() {
    # herds.
    herds=$(atomx WM_HERD $(lsw -a) | cut -d ' ' -f1 | sort -u)
    # check if any herd exist.
    if [ -n "$herds" ]; then
	# select herd target.
	herd_target=$(printf "$herds" | $XM)
	# windwos to map.
	windows_to_map=$(atomx WM_HERD $(lsw -u) | grep $herd_target | cut -d ' ' -f2)
	# windows to unmap.
	windows_to_unmap=$(atomx WM_HERD $(lsw) | grep -v $herd_target | cut -d ' ' -f2)
	# do it.
	mapw -m $windows_to_map & mapw -u $windows_to_unmap
    fi
}
	  
case $FLAG in
    -a)
	add_focused_window
	;;
    -A)
	add_all_windows
	;;
    -d)
	delete_focused_window
	;;
    -D)
	delete_all_windows
	;;
    -t)
	togle_herds
	;;
    *)
	usage    # self-explained
	;;
esac

