#!/bin/sh

## move and resize windows ##
# wms_usher.sh by @root_informatica.

. /tmp/wms_var 

FLAG=$1
# focused window.
FW=$(pfw)

usage() {
    cat<<EOF
usage:
wms_usher.sh [ -c, -f, -n, -s, -e, -o, -r, -l, -d, -u, -w, -W, -h, -H ]
-c) centered
-f) fullsize
-n) half north
-s) half south
-e) half est
-o) half west
-r) move right
-l) move left
-d) move down
-u) move up
-w) resize width -
-W) resize width +
-h) resize height -
-H) resize height +
EOF
}

# half north value
half_north() {
    x=0
    y=0
    w=$SW
    h=$((SH / 2 - BW))
    wtp $x $y $w $h $FW
}

# half south value
half_south() {
    x=0
    y=$((SH / 2 + BW))
    w=$SW
    h=$((SH / 2 - BW))
    wtp $x $y $w $h $FW
}

# half est value
half_est() {
    x=$((SW / 2 + BW))
    y=0
    w=$((SW / 2 - BW))
    h=$SH
    wtp $x $y $w $h $FW
}

# half west value
half_west() {
    x=0
    y=0
    w=$((SW / 2 - BW))
    h=$SH
    wtp $x $y $w $h $FW
}

# centered function.
center() {
    # windows width.
    window_width=$((SW * WP / 100))
    # windows height.
    window_height=$((SH * WP / 100))
    x=$(((SW - window_width) / 2))
    y=$(((SH - window_height) / 2))
    wtp $x $y $window_width $window_height $FW
}

    # fulsize function
fullsize() {
    # original size and position.
    original_sp=$(atomx WM_FULL_SIZE $FW)
    # fullsize width.
    fs_width=$((RW - BW * 2))
    # fullsize height.
    fs_height=$((RH - BW * 2))
    x=0 
    y=0

    # if atom exist and it is in fullsize
    if [ -n "$original_sp" ] & [ "$(wattr wh $FW)" = "$fs_width $fs_height" ]; then
	# restore size and position.
        wtp $original_sp $FW
	# remove atom from window.
        atomx -d WM_FULL_SIZE $FW
	
    else    # if it's not atom and it's not in fullsize
	# create atom and save original size and position.
        atomx WM_FULL_SIZE="$(wattr xywh $FW)" $FW
	# fullsize it.
        wtp $x $y $fs_width $fs_height $FW
    fi
}

case $FLAG in
    -c)
	center
	;;
    -f)
	fullsize
	;;
    -n)
	half_north
	;;
    -s)
	half_south
	;;
    -e)
	half_est
	;;
    -o)
	half_west
	;;
    -r)    # move rigth.
	wmv 10 0 $FW
	;;
    -l)    # move left.
	wmv -10 0 $FW
	;;
    -d)    # move down.
	wmv 0 10 $FW
	;;
    -u)    # move up.
	wmv 0 -10 $FW
	;;
    -w)    # resize width -.
        wrs -10 0 $FW
        ;;
    -W)    # resize width +.
        wrs 10 0 $FW
        ;;
    -h)    # resize higth -.
        wrs 0 -10 $FW
        ;;
    -H)    # resize hight +.
        wrs 0 10 $FW
        ;;
    *)
	usage
	;;
esac
