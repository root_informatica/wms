#!/bin/sh

## use the windows on the desktop as screensavers by moving them randomly ##
# wms_screensaver.sh by @root_informatica.

. /tmp/wms_var 

FLAG=$1
# focused window.
FW=$(pfw)
# windows width = 80% of the screen. 
WINDOW_WIDTH=$((SW * 80 / 100))
# windows height = 80% of the screen.
WINDOW_HEIGHT=$((SH * 80 / 100))
# max X coordinate.
MAX_X=$((SW - WINDOW_WIDTH))
# max Y coordinate.
MAX_Y=$((SH - WINDOW_HEIGHT))
# refresh freq.
FREQ=20

usage() {
    cat<<EOF
usage:
wms_screensaver.sh [ -a, -f ]
-a) all windows
-f) focused window
EOF
}

# random movement for all windows.
all_windows() {
    while true; do
	for wid in $(lsw); do
	    # random x coordinate.
	    x=$(shuf -i 0-$MAX_X -n 1)
	    # random y coordinate.
	    y=$(shuf -i 0-$MAX_Y -n 1)
	    wtp $x $y $WINDOW_WIDTH $WINDOW_HEIGHT $wid
	done
	sleep $FREQ
    done
}

# random movement for the focused window
focused_window() {
    while true; do
	x=$(shuf -i 0-$MAX_X -n 1)
	y=$(shuf -i 0-$MAX_Y -n 1)
	wtp $x $y $WINDOW_WIDTH $WINDOW_HEIGHT $FW
	sleep $FREQ
    done
}

case $FLAG in
    -a)
	all_windows
	;;
    -f)
	focused_window
	;;
    *)
	usage
	;;
esac
